$(document).ready(function() {

  $('.section__slide').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    dots: true,
    arrows: false,
    infinite : true,
    pauseOnHover : false,
    //centerMode: true,
  });

  //menu
  $(".header__menu__menu").click(function() {
    $('.header__menu__list').addClass('open');
    $('.header__menu__overlay').addClass('open');
  });
  $(".menu__close").click(function() {
    $('.header__menu__list').removeClass('open');
    $('.header__menu__overlay').removeClass('open');
  });
  $(".header__menu__overlay").click(function() {
    $('.header__menu__list').removeClass('open');
    $('.header__menu__overlay').removeClass('open');
  });

  //Search
  $(".header__menu__search").click(function() {
    $('.header__search').addClass('open');
  });
  $(".header__search__close").click(function() {
    $('.header__search').removeClass('open');
  });

});
//
